from rest_framework.views import APIView
from rest_framework.response import Response
from ABC.models import Cliente, Salon
from .serializers import ClienteSerializers, SalonSerializers, VistaGeneralSerializers
from rest_framework import status
from django.http import Http404
from rest_framework import generics


# Create your views here.



#Usuarios
class ListUserApi(APIView):


    def get(self, request, format=None):
        '''Retorna la lista de los clientes'''
        clientes = Cliente.objects.all()
        serializer = ClienteSerializers(clientes, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


    def post(self, request, format=None):
        serializer = ClienteSerializers(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




    

class UsuarioDetailApi(APIView):
    

    def get_object(self, pk):
        try:
            return Cliente.objects.get(pk=pk)
        except Cliente.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        clientes = self.get_object(pk)
        serializer = ClienteSerializers(clientes)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        clientes = self.get_object(pk)
        serializer = ClienteSerializers(clientes, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        clientes = self.get_object(pk)
        clientes.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)





#Salones
class ListSalonApi(APIView):
    

    def get(self, request, format=None):
        '''retornar la lista de los salones'''
        salones = Salon.objects.all()
        serializer = SalonSerializers(salones, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    
    def post(self, request, format=None):
        serializer = SalonSerializers(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




class SalonDetailApi(APIView):
    

    def get_object(self, pk):
        try:
            return Salon.objects.get(pk=pk)
        except Salon.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        salones = self.get_object(pk)
        serializer = SalonSerializers(salones)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        salones = self.get_object(pk)
        serializer = SalonSerializers(salones, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        salones = self.get_object(pk)
        salones.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)





#VistaGeneral
class VistaGeneralApi(APIView):

    def get(self, request, format=None):
        '''retornar tabla de usuarios con salones confirmados'''
        vista = Salon.objects.filter(estado="Confirmados")
        serializer = VistaGeneralSerializers(vista, many=True)


        return Response(serializer.data, status=status.HTTP_200_OK)







    

    