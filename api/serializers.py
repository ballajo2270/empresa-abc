from rest_framework import serializers
from ABC.models import Cliente, Salon
from django.utils.timezone import now



class ClienteSerializers(serializers.ModelSerializer):

    class Meta:
        model = Cliente
        fields = '__all__'



class SalonSerializers(serializers.ModelSerializer):


    class Meta:
        model = Salon
        fields = '__all__'



class VistaClientSelrializers(serializers.ModelSerializer):

    class Meta:
        model = Cliente
        fields = ['nombre', 'identificacion', 'telefono', 'ciudad']



class VistaGeneralSerializers(serializers.ModelSerializer):

    cliente_id = VistaClientSelrializers()

    class Meta:
        model = Salon
        fields = ['fecha_evento', 'cantidad_personas', 'motivo', 'cliente_id']



    
    