from django.urls import path
from .views import ListUserApi, ListSalonApi, VistaGeneralApi, UsuarioDetailApi, SalonDetailApi

urlpatterns = [
        path('', ListUserApi.as_view()),
        path('ListSalon/', ListSalonApi.as_view()),
        path('VistaGeneral/', VistaGeneralApi.as_view()),
        path('UsuarioDetail/<int:pk>/', UsuarioDetailApi.as_view()),
        path('SalonDetail/<int:pk>/', SalonDetailApi.as_view()),
        
]