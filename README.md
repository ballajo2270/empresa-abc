### ¿Cómo instalar el proyecto?

1. Crear entorno virtual
```
python -m venv .evn
```

2. Instalar requeriment
```
pip install -r requirements.txt
```

3. Correr proyecto
```
python manage.py runserver
```



### ¿Cómo usar el proyecto?

1. Ingresar a través de la interfaz gráfica:
```
http://127.0.0.1:8000/template/
```

2. Ingresar a través de la API
```
http://127.0.0.1:8000/api/
```
