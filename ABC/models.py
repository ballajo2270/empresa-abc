from django.db import models
from datetime import datetime

# Create your models here.

class Cliente(models.Model):

    identificacion = models.CharField(max_length=15, blank=False)
    nombre = models.CharField(max_length=50, blank=False)
    apellido = models.CharField(max_length=50, blank=False)
    telefono = models.CharField(max_length=20, blank=False)
    correo = models.CharField(max_length=50, blank=False)
    departamento = models.CharField(max_length=20, blank=False)
    ciudad = models.CharField(max_length=20, blank=False)
    edad = models.IntegerField(blank=False)

    class Meta:
        db_table = 'Cliente'

    def __str__(self):
        return self.nombre


class Salon(models.Model):
    MOT_CHOICES = (
        ('Evento empresarial', "Evento empresarial"),
        ('despedida empresa', "despedida empresa"),
        ('desayuno comercial', "desayuno comercial"),
        ('almuerzo', "almuerzo"),
    )

    ESTADO_CHOICES = (
        ('Confirmados', "Confirmado"),
        ('No confirmados', "No confirmado")
    )

    cliente_id = models.ForeignKey(Cliente, on_delete = models.CASCADE, blank=False)
    fecha_evento = models.DateTimeField(blank=False)
    cantidad_personas = models.IntegerField(blank=False)
    motivo = models.CharField(max_length=18 ,choices=MOT_CHOICES, blank=False)
    observaciones = models.TextField(blank=False)
    estado = models.CharField(max_length=14 ,choices=ESTADO_CHOICES, blank=False)


    class Meta:
        db_table = 'Salon'



