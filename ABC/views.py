from django.shortcuts import render
from .models import Cliente, Salon
from django.views.generic import ListView, CreateView, TemplateView, UpdateView
from django.views.generic.edit import DeleteView
from .forms import ClienteForm, SalonForm, VistaFecha
from django.urls import reverse_lazy
from datetime import datetime

# Create your views here.

#listvew
class ClienteListView(ListView):
    model = Cliente
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'clientes'
        return context

class SalonesListViews(ListView):
    model = Salon
    template_name = 'salones.html'



#createview
class CrearUsuarioCreateView(CreateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'crearUsuario.html'
    success_url = reverse_lazy('cliente') 


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Creacion de clientes'
        return context



class CrearSalonCreateView(CreateView):
    model = Salon
    form_class = SalonForm
    template_name = 'crearsalon.html'
    success_url = reverse_lazy('salon') 





#Vista general
class VistaGeneralListViews(TemplateView):
    template_name = 'vistaGeneral.html'
    

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        fecha_inicial = self.request.GET.get('fecha_inicial')
        fecha_final = self.request.GET.get('fecha_final')
        context['salon'] = Salon.objects.filter(estado="Confirmados")


        if fecha_inicial and fecha_final:
            fecha_inicial = datetime.strptime(fecha_inicial,'%Y-%m-%d')
            fecha_final = datetime.strptime(fecha_final,'%Y-%m-%d')

            
            context['salon'] = Salon.objects.filter(estado="Confirmados", fecha_evento__range=(fecha_inicial, fecha_final))
            
       
        return context
        







#UpdateView

class ClienteUpdateView(UpdateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'crearUsuario.html'
    success_url = reverse_lazy('cliente') 


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Actualizar de clientes'
        return context


class CrearSalonUpdateView(UpdateView):
    model = Salon
    form_class = SalonForm
    template_name = 'crearsalon.html'
    success_url = reverse_lazy('salon') 


#DeleteView
class SalonDeleteView(DeleteView):
    model = Salon
    template_name = 'deleteSalon.html'
    success_url = reverse_lazy('salon')


#DeleteView
class ClienteDeleteView(DeleteView):
    model = Cliente
    template_name = 'deleteClient.html'
    success_url = reverse_lazy('cliente')
