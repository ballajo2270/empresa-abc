from django.contrib import admin

# Register your models here.
from .models import Cliente, Salon

#cliente
admin.site.register(Cliente)


#salon
admin.site.register(Salon)

