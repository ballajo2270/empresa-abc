from django.urls import path
from ABC.views import ClienteListView, SalonesListViews, CrearUsuarioCreateView,CrearSalonCreateView, VistaGeneralListViews, ClienteUpdateView,CrearSalonUpdateView,SalonDeleteView,ClienteDeleteView

urlpatterns = [
    path('', ClienteListView.as_view(), name='cliente'),
    path('salones/',SalonesListViews.as_view(), name='salon'),
    path('CrearUsuario/', CrearUsuarioCreateView.as_view(), name='CrearUsuario'),
    path('CrearSalon/', CrearSalonCreateView.as_view(), name='CrearSalon'),
    path('VistaGeneral/', VistaGeneralListViews.as_view(), name='VistaGeneral'),
    path('ActualizarClient/<int:pk>/', ClienteUpdateView.as_view(), name='ActualizarClient'),
    path('ActualizarSalon/<int:pk>/', CrearSalonUpdateView.as_view(), name='ActualizarSalon'),
    path('ElimminarSalon/<int:pk>/', SalonDeleteView.as_view(), name='EliminarSalon'),
    path('ElimminarCliente/<int:pk>/', ClienteDeleteView.as_view(), name='EliminarCliente'),
]