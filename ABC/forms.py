from django import forms
from .models import Cliente, Salon
from django.forms import Textarea






class ClienteForm(forms.ModelForm):

    edad = forms.ChoiceField(choices=[(x, x) for x in range(18, 90)])

    class Meta:
        model = Cliente
        fields = '__all__'

class SalonForm(forms.ModelForm):

    class Meta:
        model = Salon
        fields = '__all__'
        widgets = {
            'observaciones': Textarea(attrs={'cols': 20, 'rows': 10}),
            'fecha_evento': forms.DateTimeInput(attrs={'class': 'datetime-input'})
        }

    
class VistaFecha(forms.Form):
    
    fecha_inicial = forms.DateField()
    fecha_final = forms.DateField()